# deployment-task

## Docker Image:
	created a docker image of the demo application mentioned in 
	` https://gitlab.com/siddhant1223/kubernetes-tasks/-/tree/main/Django_Project `
	
	public image name: `` siddhant1223/demo_django:v1 ``
	
## Kubernetes Environment
	Created a 3 node cluster with minikube with HPA scaling enabled, 
	minikube start --driver=kvm2 -n 3

	minikube addons enable ingress 
	minikube addons enable metrics-server	
	
	
	created manifest files 
 		- deployment.yml
 		- clusterIp.yml
 		- nodeport.yml
 		- ingress.yml
 		- hpa.yml

    applied manifest
 	`kubectl apply -f .`
 
    added minikube ip to the host file 
 	`echo "$(minikube ip) test-django.info"	| sudo tee -a /etc/hosts `	
 		
	
	entire deployment files are stored in the "kubernetes-file" directory
	` https://gitlab.com/siddhant1223/kubernetes-tasks/-/tree/main/kubernetes-files `
	
	for ingress - I used the available addons of ingress in minikube and carried the task forward, tried using an external nginx-ingress the configuration is mentioned in 
`	https://gitlab.com/siddhant1223/kubernetes-tasks/-/blob/main/kubernetes-files/ingress-nginx.yml`
	
## Load simulating
created an seperate instance to generate load using jmeter the load.jmx file in ``https://gitlab.com/siddhant1223/kubernetes-tasks/-/blob/main/jmeter-script/load.jmx`` Demonstrates a simple load dispatcher, which runs 	100 threads with 150 loop count and 1 second ramp-up period
